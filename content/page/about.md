---
title: About Sheddington
subtitle: Looking for a place in Teddington
comments: false
---

![Logo](/img/avatar-icon.png)


# Story

Dr Chris Manning, now retired, worked as a GP locally for many years.  “There are lots of local organisations providing some fantastic services. However, I have worked with many people who still fall between the gaps and I know would benefit from being part of something different. Not a service, but a co-created  community. We want to make a contribution that will compliment and signpost, when we can, other services.”    

So Chris started talking to people about a very simple idea:         "Doing stuff that we like doing … together”.
Fast forward months of research, building a team and listening to people and “Sheddington” was born. 

The benefits are potentially vast. “Firstly, there is the friendship and joy of sharing interests and learning.  Second, we know that this type of activity can improve and protect people’s physical and mental health. “
Inclusivity, a “shed” for all, is at the heart of Sheddington.  “People need places they can go to as themselves and just be themselves” Laura, Sheddington team member.  
