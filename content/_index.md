![Logo](/img/avatar-icon.png)


# What are we?
A Community Space (“Shed”) in Teddington                                                                     
Where people can meet up, make stuff and mend things 
Working together or alongside each other
Inclusive, Secular & Not for Profit
# What do we do ?
DIY, fixing electronics/motorbikes, woodwork, crafts, etc

Mindfulness, walks, lectures, book club, etc

A friendly and safe space with tea, coffee & facilities,

**Get in Touch** ,                                                 Be  part of the Sheddington community. 

E:  <info@sheddington.org>
         **By contacting us you are only expressing interest.**              		
Information held on the mailing list will not be shared with any 3rd parties

If you know of place please reach out. In the mean time, [subscribe to our newsletter](https://facebook.us19.list-manage.com/subscribe/post?u=73a7767f976ca0a5eeef92d04&amp;id=af317b51f9)



Our [Temporary Location is Richmond MakerLabs](https://richmondmakerlabs.uk/) we meet every Friday from 14hrs to 17hrs. But we are looking for a location in Teddington. 

[Please share our flyer](docs/261118 Sheddington Promo WEB.pdf) 

# Contacts

* For a general chat: speak to Chris: <info@sheddington.org>.
* If you can offer a location please speak to: <location@sheddington.org>.
* If you want to take ownership of one of our social medias please talk to Tracy: <social@sheddington.org>.
* If you can provide funding please talk to Hannah: <funding@sheddington.org>.
* If you want to be kept informed of our progress via email please [subscribe to our newsletter](https://facebook.us19.list-manage.com/subscribe/post?u=73a7767f976ca0a5eeef92d04&amp;id=af317b51f9) (you will recieve emails from news at sheddington.org).


